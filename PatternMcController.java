package com.advento.starpatternms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.advento.starpatternms.service.patternMcService;

@RestController
public class PatternMcController {

	@Autowired
	patternMcService patternmc;

	@Qualifier(value = "PatternSeriveImpl")
	@GetMapping(value = "/daimond")
	public ResponseEntity<String> daimond(@RequestParam int length) {
		String result = patternmc.Daimond(length);
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

	
	  @GetMapping(value = "/pyramid length") public ResponseEntity<String>
	  pyramid(@RequestParam int length) { String pyramid =
	  patternmc.Pyramid(length);
	  
	  ResponseEntity<String> entity = new ResponseEntity<>(pyramid, HttpStatus.OK);
	  return entity; }
	  
	 	@GetMapping(value = "/rightangle")
	public ResponseEntity<String> rightAngle(@RequestParam int length) {
		String rightAngle = patternmc.rightAngle(length);
		ResponseEntity<String> entity = new ResponseEntity<String>(rightAngle, HttpStatus.OK);
		return entity;
	}

	@GetMapping(value = "/reversepyramid")
	public ResponseEntity<String> reversePyramid(@RequestParam int length) {
		String reversePyramid = patternmc.reversePyramid(length);
		ResponseEntity<String> entity = new ResponseEntity<>(reversePyramid, HttpStatus.OK);
		return entity;
	}

	@GetMapping(value = "/reversePyramidUsingWhile")

	public ResponseEntity<String> reversePyramidUsingWhile(@RequestParam int length) {
		String reversePyramidUsingWhile = patternmc.reversePyramidUsingWhile(length);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(reversePyramidUsingWhile, HttpStatus.OK);
		return responseEntity;

	}

	@GetMapping(value = "/reversePyramidDoUsingWhile")
	public ResponseEntity<String> reversePyramidDoUsingWhile(@RequestParam int length) {
		String reversePyramidUsingDoWhile = patternmc.reversePyramidUsingDoWhile(length);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(reversePyramidUsingDoWhile, HttpStatus.OK);
		return responseEntity;

	}
	@GetMapping(value = "/pyramidusingwhile")
	public ResponseEntity<String> PyramidUsingWhile(@RequestParam int length) {
		String pyramidusingdowhile = patternmc.pyramidUsingWhile(length);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(pyramidusingdowhile, HttpStatus.OK);
		return responseEntity;

	}
	@GetMapping(value = "/pyramidusingdowhile")
	public ResponseEntity<String> PyramidUsingDoWhile(@RequestParam int length) {
		String pyramidusingdowhile = patternmc.pyramidUsingDoWhile(length);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(pyramidusingdowhile, HttpStatus.OK);
		return responseEntity;

	}
	@GetMapping(value = "/daimondusingwhile")
	public ResponseEntity<String> daimondUsingWhile(@RequestParam int length) {
		String daimondusingwhile = patternmc.daimondUsingWhile(length);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(daimondusingwhile, HttpStatus.OK);
		return responseEntity;

	}
	
}
